const Bicicleta= require ('../models/bicicleta');

exports.bicicleta=function(req,res){
    res.render('bicicleta/index',{bicis:Bicicleta.allBicicletas});
}

exports.bicicletaCreateGet=function(req,res){
    res.render('bicicleta/create');
}

exports.bicicletaCreatePost=function(req,res){
    const bici=new Bicicleta(req.body.id,req.body.color,req.body.modelo,[req.body.lat,req.body.long]);
    console.log(bici);
    Bicicleta.addBicicleta(bici);
    res.redirect('/bicicletas')
}

exports.bicicletaUpdateGet=function(req,res){
    res.render('bicicleta/update',{bici:Bicicleta.findBicicleta(req.params.id)});
}

exports.bicicletaUpdatePost=function(req,res){
    const bici=new Bicicleta(req.body.id,req.body.color,req.body.modelo,[req.body.lat,req.body.long]);
    Bicicleta.updateBicicleta(bici);
    res.redirect('/bicicletas')
}

exports.bicicletaDelete=function(req,res){
    console.log(req.body.id);
    Bicicleta.deleteBicicleta(req.body.id);
    res.redirect('/bicicletas');
}