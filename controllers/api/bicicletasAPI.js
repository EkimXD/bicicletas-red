const Bicicletas=require ('../../models/bicicleta');

exports.bicicleta_show=function(req,res){
    res.status(200).json({
        bicicletas:Bicicletas.allBicicletas,
    });
}

exports.bicicleta_create=function(req,res){
    const bici=new Bicicletas(req.body.id,req.body.color,req.body.modelo,[req.body.lat,req.body.long]);
    Bicicletas.addBicicleta(bici);
    res.status(200).json({
        bicicleta:bici,
    })
}

exports.bicicleta_delete=function(req,res){
    Bicicletas.deleteBicicleta(req.params.id);
    res.status(200).json({
        bicicleta:"deleted",
    })
}


exports.bicicleta_update=function(req,res){
    const bici=new Bicicletas(req.body.id,req.body.color,req.body.modelo,[req.body.lat,req.body.long]);
    Bicicletas.updateBicicleta(bici);
    res.status(200).json({
        bicicleta:bici,
    })
}