let Bicicleta=function(id,color, modelo, coordenadas){
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.coordenadas=coordenadas;
};

Bicicleta.prototype.toString=function(){
    return `id:${this.id}, color:${this.color}, modelo:${this.modelo}, coordenadas:${this.coordenadas}`;
};

Bicicleta.allBicicletas=[];
Bicicleta.addBicicleta=function(bicicleta){
    Bicicleta.allBicicletas.push(bicicleta);
};

Bicicleta.deleteBicicleta=function(id){
    this.allBicicletas.forEach((value, position)=>{
        if(value.id==id){
            this.allBicicletas.splice(position,1);
        }
    })
}

Bicicleta.findBicicleta=function(id){
    const bicicletas=this.allBicicletas.find(x=>x.id=id);
    if(bicicletas){
        return bicicletas;
    }else{
        throw new Error(`No se encontro el elemento con id=${id}`);
    }
}

Bicicleta.updateBicicleta=function(bici){
    const bicicleta=this.findBicicleta(bici.id);
    const bicicletas=this.allBicicletas.indexOf(bicicleta);
    if(bicicletas>=0){
        this.allBicicletas[bicicletas]=bici;
    }else{
        throw new Error(`No se encontro el elemento con id=${id}`);
    }
}

let a=new Bicicleta(1,'rojo','alguno',[-0.2298500, -78.5249500]);
let b=new Bicicleta(2,'negra','alguno',[-0.2288500, -78.5249500])

Bicicleta.addBicicleta(a);
Bicicleta.addBicicleta(b);

module.exports=Bicicleta;