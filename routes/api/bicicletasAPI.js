const express=require('express');
const router=express.Router();
const bicicletas=require ('../../controllers/api/bicicletasAPI');

router.get('/',bicicletas.bicicleta_show);
router.post('/',bicicletas.bicicleta_create);
router.delete('/:id',bicicletas.bicicleta_delete);
router.post('/:id',bicicletas.bicicleta_update);

module.exports=router;