const express=require('express');
const router=express.Router();

const bicicletaController=require('../controllers/bicicleta');

router.get('/',bicicletaController.bicicleta);
router.get('/create',bicicletaController.bicicletaCreateGet);
router.post('/create',bicicletaController.bicicletaCreatePost);
router.post('/:id/delete',bicicletaController.bicicletaDelete);
router.get('/:id/update',bicicletaController.bicicletaUpdateGet);
router.post('/:id/update',bicicletaController.bicicletaUpdatePost);

module.exports=router;