var map = L.map('main_map').setView([-0.2298500, -78.5249500], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


fetch("/api")
    .then(
        response=>{
            return response.json();
        }
    ).then(
        response=>{
            console.log(response);
            response.bicicletas.forEach(element => {
                L.marker(element.coordenadas,{title:element.id}).addTo(map);
            });
        }
    ).catch(
        error=>{
            console.log(error);
        }
    )